# Dependencies for the hpx-matlab-client

## Contents
- [C library Dependencies](#c-library-dependencies)
    - [Prep](#prep)
    - [libzmq](#libzmq)
    - [msgpack-c](#msgpack-c)
- [MEX Libraries](#mex-libraries)
    - [matlab-zmq](#matlab-zmq)
    - [msgpack-matlab2](#msgpack-matlab2)

`hpx-matlab-client` relies on the MEX libraries `matlab-zmq` and `msgpack-matlab2`.
 These rely on the C libraries `libzmq` and `msgpack-c`, respectively. This file
 explains how to build those dependencies

## C library Dependencies

The C development libraries for ZeroMQ and MessagePack are required. 

On **Ubuntu** & derivatives, the package repository versions should be sufficient:
* libzmq - `libzmq3-dev`
* msgpack-c - `libmsgpack-dev`

Then jump to the [MEX Libraries](#mex-libraries) section.

On **CentOS/RedHat**, it's better to build the latest version of each from source.

### Prep

First, create a sources dir and install dir
```bash
SOURCES=~/src  # or as desired
mkdir -p $SOURCES
INSTALLDIR=/usr/local
```

### libzmq

Get and install libzmq from source:
```bash
# libzmq
cd $SOURCES
git clone https://github.com/zeromq/libzmq.git  # 4.3.1 as of this writing.
cd libzmq
mkdir build
cd build
cmake -DCMAKE_INSTALL_PREFIX=${INSTALLDIR} ..
make -j4
sudo make install
```
### msgpack-c

Download and the
[current release > 2.1](https://github.com/msgpack/msgpack-c/releases) to
`$SOURCES` and install:

```bash
cd $SOURCES
tar -xvf msgpack-3.1.1.tar.gz  # As appropriate
cd msgpack-3.1.1
mkdir build
cd build
# build setup
cmake -DCMAKE_INSTALL_PREFIX=${INSTALLDIR} ..
# build the software
make
# install to /usr/local/ (bin, lib, etc.)
sudo make install
```

On some systems, `/usr/local/lib` is not a default search location for shared object libraries.
 You can check by running `sudo ldconfig -v | grep msgpack` and see if `libmsgpackc.so.2` shows up.
 If not, create a new file in `/etc/ld.so.conf.d` with the path to `/usr/local/lib`, and then run `ldconfig` again.

```bash
sudo echo /usr/local/lib > /etc/ld.so.conf.d/usr-local-lib.conf
sudo ldconfig -v | grep msgpack
```

## MEX Libraries

Now, we can build the MEX libraries using the C libraries

### [matlab-zmq](https://github.com/randallpittman/matlab-zmq)
Currently the original version of this library doesn't unpack multipart
messages to a cell array (and thus the parts are not distinguishable). I made
a fork with the fix that I hope will be pulled into upstream. In the meantime,
use my fork.

1. Download:

```bash
# get latest matlab-zmq
cd $SOURCES
git clone https://github.com/randallpittman/matlab-zmq.git
cd matlab-zmq
```

2. It may be necessary to edit `config.m` with the correct library name and lib and
include paths, especially if you manually built and installed libzmq. For example,
 on my Linux Mint machine, my `config.m` file would be something like this:

```matlab
% ZMQ library filename
ZMQ_COMPILED_LIB = 'libzmq.a';

% ZMQ library path
ZMQ_LIB_PATH = '/usr/lib/x86_64-linux-gnu';

% ZMQ headers path
ZMQ_INCLUDE_PATH = '/usr/include/';
```

To find where `libzmq.a` is located try running
 `find /usr -type f -name "libzmq.a"`.

3. Build the software in MATLAB:

```bash
matlab -r make.m
```

The MATLAB package is now in the `lib/+zmq` folder.

### msgpack-matlab2
This is another C Mex library I forked, but I made more more extensive changes
to this one. Building is pretty easy.

```bash
cd $SOURCES
git clone https://github.com/randallpittman/msgpack-matlab2.git
cd msgpack-matlab2
mex -lmsgpackc -I$INSTALLDIR/include msgpack.cc
```

