%% hpx-matlab-client
% This script is intended to demonstrate how one may receive and process radar 
% data from the |hpx-radar-server| program.
%% onCleanup Setup
% |onCleanup| creates an object that automatically calls some function when 
% it is deleted. We can use this to make sure the ZeroMQ publisher is closed before 
% re-running the script and/or when MATLAB closes (even if there is an error). 
% Further detail on |zmq_cleanup()| is shown under that function definition, later 
% in this file.

% onCleanup function called if cleanupObj exists.
clear cleanupObj
% call zmq_cleanup('clean') when cleanupObj is destroyed
cleanupObj = onCleanup(@() zmq_cleanup('clean'));

%% Set up sockets
% |endpoint| is the "address" of the ZeroMQ pub/sub socket |hpx-radar-server| 
% is using.
%%
endpoint = 'ipc:///shared/sockets/WIMR.sock';
%% 
% Every time we use ZeroMQ, we create a 'context', create sockets in that context, and then
% connect or bind to the sockets. In this case, the |hpx-radar-server| app is sending data out
% on ZeroMQ Publisher sockets, so for our client we can get the data with ZeroMQ Subscriber
% sockets. We can create separate ZeroMQ subscriber sockets at the same endpoint that
% subscribe to different message types.

context = zmq.Context();
zmq_cleanup('setcontext', context);  % context will be deleted on cleanup
ray_sub = context.socket('ZMQ_SUB');
info_sub = context.socket('ZMQ_SUB');
% automatically close subscribers on cleanup
zmq_cleanup('addsocket', ray_sub);
zmq_cleanup('addsocket', info_sub);
% Set the message types that the subscribers will allow in.
ray_sub.set('ZMQ_SUBSCRIBE', 'RAY');
info_sub.set('ZMQ_SUBSCRIBE', 'CONFIG');
info_sub.set('ZMQ_SUBSCRIBE', 'STATUS');

%% Setup for receiving and plotting rays

% Get and parse a config message, extract useful stuff
config = get_and_unpack_msg(info_sub, endpoint, 'CONFIG');
raymsg_size = double(config.num_samples)*2 + 115;  % ray + ancillary data & key
n_azi = config.tpg_arp_frequency;
n_samp = config.num_samples;

%%% Set up figure and create a dummy plot.
plot_type = 'pol';  % 'pol': x/y axes, 'flat': azi/rg axes
fig = figure();
ax = axes('Parent', fig);
frame = zeros([n_samp, n_azi], 'uint16');
azis = zeros([1, n_azi]);
if strcmp(plot_type, 'flat')
    im = imagesc(ax, frame);
    ax.YDir = 'normal';
    ax.XLim = [1, n_azi];
    ax.XLabel.String = 'rays';
    ax.YLim = [1, n_samp];
    ax.YLabel.String = 'samples';
elseif strcmp(plot_type, 'pol')
    max_r = n_samp*3;
    X = -max_r:10:max_r;
    Y = X;
    [x_grid, y_grid] = meshgrid(X, Y);
    plot_frame = zeros(size(x_grid), 'uint16');
    [phi_plt, r_plt] = cart2pol(x_grid, y_grid);
    azi_plt = mod(90 - phi_plt*180/pi, 360);
    im = imagesc(ax, X, Y, plot_frame);
    ax.XLim = [-max_r, max_r];
    ax.YLim = [-max_r, max_r];
end
axis image;

%% Acquire ray messages

% connecting to the endpoint means we're "subscribed" and ray messages start queueing up to be
% processed.
ray_sub.connect(endpoint);
R = 3*(1:n_samp);
nrots_done = 0;
nrots_to_plot = 64;
last_ray_idx = uint8(0);  % track new rotation
lasts_tstamp = '';
first_rot = true;
while nrots_done < nrots_to_plot
    % A multi-part 0MQ message has an "envelope", or key, and a payload. This socket
    % subscribed to the key RAY, so we should only get those types of messages.
    parts = ray_sub.recv_multipart(raymsg_size, 'ZMQ_DONTWAIT');
    if numel(parts) > 1
        % we use msgpack to unpack the uint8 array payload (parts{2}) to a struct.
        ray = msgpack('unpack', parts{2});
        % At first skip rays until we get to the first ray of a rotation
        if first_rot && ray.ray_idx > 0, continue, end
        first_rot = false;
        if ray.ray_idx < last_ray_idx
            % Ok, this is a new rotation, so plot the last now before replacing the data in
            % frame
            title(last_tstamp);
            if strcmp(plot_type, 'flat')
                im.CData = frame;
            elseif strcmp(plot_type, 'pol')
                plot_frame = interp2(azis, R, double(frame), azi_plt, r_plt);
                im.CData = plot_frame;
                im.AlphaData = ~isnan(plot_frame);
            end
            drawnow;
            nrots_done = nrots_done + 1;
        end
        % here we actually save the ray data.
        % ray.azimuth is 0-65535 (2^16 - 1)
        azis(ray.ray_idx+1) = ray.azimuth*360/2^16;
        % ray.data is a byte buffer that needs to be typecast to an array of uint16s.
        frame(:, ray.ray_idx+1) = typecast(ray.data, 'uint16');
        % ray.proc_time is that time when hpx-radar-server processed and sent out that ray
        % (as opposed to the time the ray was actually received, which was some small time
        % earlier.)
        last_tstamp = ray.proc_time;

        % Warn if we skipped any rays. This would mean we're probably processing too slowly.
        ray_diff = double(ray.ray_idx) - last_ray_idx;
        if ray_diff > 1
            warning('Skipped %d rays at ray %d.', ray_diff-1, ray.ray_idx);
        end
        last_ray_idx = ray.ray_idx;
    end
end
ray_sub.disconnect(endpoint);

%% Make sure everything is closed.
clear cleanupObj  % onCleanup function called.


%% Ancillary Functions
%%% get_and_unpack_msg()
% This function connects a socket to an endpoint, gets a single message, and closes the
% socket.
function data = get_and_unpack_msg(sub, endpoint, envelope)
% get_and_unpack_msg(sub, envelope_address, endpoint)
% Connect |sub| to |endpoint| and wait for, capture, and parse a multi-part
% message with |envelope| as the first part and a MessagePack map as the second
% part.
%
% Inputs:
%   sub - ZeroMQ socket
%   endpoint - ZeroMQ endpoint string, e.g. 'ipc://config.socket'
%   envelope - ZeroMQ envelope string, that is the key that names the type
%              of message.
% Returns:
%   data - struct of keys and values, extracted from the map
sub.connect(endpoint);
% Aquire message
data = [];
while isempty(data)
    parts = sub.recv_multipart(1024, 'ZMQ_DONTWAIT');
    if isempty(parts), continue, end
    if numel(parts) > 1
        if strcmp(char(parts{1}), envelope)
            data = msgpack('unpack', parts{2});
        end
    end
end
sub.disconnect(endpoint);
end

%%% zmq_cleanup()
% This function has some persistent variables so it acts sort of like an instance of a class.
% The 0mq context and sockets get "registered" with the 'setcontext' and 'addsocket' calls,
% and then are closed/deleted when the function is called with the 'clean' argument.
function zmq_cleanup(varargin)
% zmq_cleanup - cleanup function stores context and sockets. Used with
% onCleanup.
%
% Inputs (varargin):
%   'setcontext', context
%     Set the context to be deleted when 'clean' is called.
%   'addsocket', socket
%     Add a socket to be closed before the context is deleted.
%   'clean'
%     Close the sockets and then delete the context.
persistent context sockets;
vi = 0;
while vi < numel(varargin)
    vi = vi + 1;
    argi = varargin{vi};
    if ~ischar(argi)
        warning('Arg %d of zmq_cleanup should be a string but is a %s.', vi, class(argi));
        continue
    end
    switch argi
        case 'setcontext'
            context = varargin{vi + 1};
            vi = vi + 1;
        case 'addsocket'
            if isempty(sockets)
                sockets = {};
            end
            sockets = [sockets, varargin(vi + 1)];
            vi = vi + 1;
        case 'clean'
            if ~isempty(sockets)
                for si = numel(sockets):-1:1
                    sockets{si}.close()
                end
            end
            if ~isempty(context)
                context.delete();
            end
            clear sockets context
        otherwise
            warning('Unexpected argument to zmq_cleanup: %s', argi);
    end  % switch
end  % while ...
end  % ZMQ_CLEANUP
